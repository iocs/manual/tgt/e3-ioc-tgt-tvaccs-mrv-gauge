# IOC for MRV and MHV vacuum gauge controllers and gauges

## Used modules

*   [vac_ctrl_mks946_937b](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_mks946_937b)


## Controlled devices

*   Tgt-MRV:Vac-VEG-010
    *	Tgt-MRV:Vac-VGP-011
    *	Tgt-MRV:Vac-VGP-021
    *   Tgt-MRV:Vac-VGC-011
    *	Tgt-MRV:Vac-VGC-012


*   Tgt-MRV:Vac-VEG-020
    *	Tgt-MRV:Vac-VGP-024
    *	Tgt-MRV:Vac-VGP-026
    *	Tgt-MRV:Vac-VGP-098


*   Tgt-MRV:Vac-VEG-090
    *	Tgt-MRV:Vac-VGP-091
    *	Tgt-MRV:Vac-VGP-093