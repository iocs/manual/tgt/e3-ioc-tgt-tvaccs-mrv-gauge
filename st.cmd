#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_mks946_937b
#
require vac_ctrl_mks946_937b,4.5.2


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_mks946_937b_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: Tgt-MRV:Vac-VEG-010
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = Tgt-MRV:Vac-VEG-010, BOARD_A_SERIAL_NUMBER = 1807120652, BOARD_B_SERIAL_NUMBER = 1610311306, BOARD_C_SERIAL_NUMBER = 1611011016 , IPADDR = moxa-vac-target-mrv.cslab.esss.lu.se, PORT = 4001")

#
# Device: Tgt-MRV:Vac-VGP-011
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-MRV:Vac-VGP-011, CHANNEL = A1, CONTROLLERNAME = Tgt-MRV:Vac-VEG-010")

#
# Device: Tgt-MRV:Vac-VGP-021
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-MRV:Vac-VGP-021, CHANNEL = A2, CONTROLLERNAME = Tgt-MRV:Vac-VEG-010")

#
# Device: Tgt-PBWRV:Vac-VGC-011
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = Tgt-MRV:Vac-VGC-011, CHANNEL = B1, CONTROLLERNAME = Tgt-MRV:Vac-VEG-010")

#
# Device: Tgt-PBWRV:Vac-VGC-012
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = Tgt-MRV:Vac-VGC-012, CHANNEL = C1, CONTROLLERNAME = Tgt-MRV:Vac-VEG-010")


#
# Device: Tgt-MRV:Vac-VEG-020
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = Tgt-MRV:Vac-VEG-020, BOARD_A_SERIAL_NUMBER = 1807170627, BOARD_B_SERIAL_NUMBER = 1604281351, BOARD_C_SERIAL_NUMBER = 1504301451 , IPADDR = moxa-vac-target-mrv.cslab.esss.lu.se, PORT = 4002")

#
# Device: Tgt-MRV:Vac-VGP-024
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-MRV:Vac-VGP-024, CHANNEL = A1, CONTROLLERNAME = Tgt-MRV:Vac-VEG-020")

#
# Device: Tgt-MRV:Vac-VGP-026
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-MRV:Vac-VGP-026, CHANNEL = A2, CONTROLLERNAME = Tgt-MRV:Vac-VEG-020")

#
# Device: Tgt-MRV:Vac-VGP-098
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-MRV:Vac-VGP-098, CHANNEL = C1, CONTROLLERNAME = Tgt-MRV:Vac-VEG-020")


#
# Device: Tgt-MRV:Vac-VEG-090
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = Tgt-MRV:Vac-VEG-090, BOARD_A_SERIAL_NUMBER = 1807121126, BOARD_B_SERIAL_NUMBER = 1505070738, BOARD_C_SERIAL_NUMBER = 1609090753, IPADDR = moxa-vac-target-mrv.cslab.esss.lu.se, PORT = 4009")

#
# Device: Tgt-MRV:Vac-VGP-091
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-MRV:Vac-VGP-091, CHANNEL = A1, CONTROLLERNAME = Tgt-MRV:Vac-VEG-090")

#
# Device: Tgt-MRV:Vac-VGP-093
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = Tgt-MRV:Vac-VGP-093, CHANNEL = A2, CONTROLLERNAME = Tgt-MRV:Vac-VEG-090")





